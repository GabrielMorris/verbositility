const { arrayChunk } = require('../arrayChunk');

describe('Tests if an array is properly chunked', () => {
  test('Chunking arrays', () => {
    expect(arrayChunk([1, 2, 3, 4], 2)).toEqual([[1, 2], [3, 4]]);
    expect(arrayChunk([1, 2, 3, 4, 5], 2)).toEqual([[1, 2], [3, 4], 5]);
    expect(arrayChunk([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 4)).toEqual([
      [1, 2, 3, 4],
      [5, 6, 7, 8],
      9,
      10
    ]);
  });

  // test('Failing to chunk arrays', () => {
  //   expect(() => {
  //     arrayChunk('hello');
  //   }).toThrow('Argument is not an array');
  // });
  test('throws on octopus', () => {
    expect(() => {
      arrayChunk('octopus');
    }).toThrow();
  });
});
