const { isArray } = require('../isArray');

describe('Tests if an argument is an array', () => {
  test('arrays should return true', () => {
    expect(isArray([1, 2, 3])).toBe(true);
    expect(isArray([1, [2], 3])).toBe(true);
  });

  test('non arrays should return false', () => {
    expect(isArray(1)).toBe(false);
    expect(isArray('hello')).toBe(false);
    expect(isArray({})).toBe(false);
  });
});
