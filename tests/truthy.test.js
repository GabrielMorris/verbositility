const { truthy } = require('../truthy');

describe('Tests if a value if truthy or falsy', () => {
  test('Truthy values should be truthy', () => {
    expect(truthy(true)).toBe(true);
    expect(truthy(1)).toBe(true);
    expect(truthy('a')).toBe(true);
    expect(truthy({ test: 'hello' })).toBe(true);
  });

  test('Falsy values should be false', () => {
    expect(truthy(false)).toBe(false);
    expect(truthy(null)).toBe(false);
    expect(truthy(undefined)).toBe(false);
    expect(truthy()).toBe(false);
  });
});
