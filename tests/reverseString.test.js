const { reverseString } = require('../reverseString');

describe('Tests if a string is reversed', () => {
  test('Reversing a string', () => {
    expect(reverseString('hello')).toEqual('olleh');
  });

  test('Should throw error if not a string', () => {
    expect(() => {
      reverseString(123);
    }).toThrow();
  });
});
