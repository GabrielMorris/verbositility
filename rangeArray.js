'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
/*
* Function that creates an array up to a given number
* It takes a max bound and an optional start and step
*/
function rangeArray(maxBound, start = 0, step = 1) {
  const rangedArray = [];
  for (let i = start; i < maxBound; i += step) {
    rangedArray.push(i);
  }
  return rangedArray;
}
exports.rangeArray = rangeArray;
