'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
/*
* Function that takes a single array and returns multidimensional
* array 'chunked' into arrays of a given size
* arrayChunk([1,2], 1) => [[1],[2]]
*/
const { isArray } = require('./isArray');

function arrayChunk(array, size) {
  const returnArray = [];
  let count = 0;
  let arrayHolder = [];
  const remainderLength = array.length - (array.length % size);

  if (!isArray(array)) {
    throw new Error('Argument is not an array');
  } else {
    array.forEach((item, index) => {
      // if count < size we need to push to holder arr
      // ex: count is 1, size is 2, we push to arr
      if (count < size) {
        arrayHolder.push(item);
        count++;
      }
      // if count === size we need to push holder to return
      // and clear holder and reset count
      // ex: count is 2 size is 2, returnArr gets holder arr
      // holder is [], count is reset
      if (count === size) {
        returnArray.push(arrayHolder);
        count = 0;
        arrayHolder = [];
      }
      // if index > array.length and there is a remainder push numbers to return array
      if (index >= remainderLength) returnArray.push(item);
    });
    return returnArray;
  }
}
exports.arrayChunk = arrayChunk;
