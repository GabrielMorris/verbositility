function isArray(array) {
  return Array.isArray(array);
}

exports.isArray = isArray;
