'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
/*
* Function that returns true if x is true
* and false if it does not
*/
const exists_1 = require('./exists');
function truthy(x) {
  return x !== false && exists_1.exists(x);
}
exports.truthy = truthy;
