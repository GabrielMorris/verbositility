'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
/*
* Function that takes a value (times) and a function
* (func), creates an array of up to times and then maps
* it with the provided function
*/
const rangeArray_1 = require('./rangeArray');
function repeater(times, func) {
  return rangeArray_1.rangeArray(times).map(func);
}
exports.repeater = repeater;
