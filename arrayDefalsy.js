'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
/*
* Function for removing falsy values from an array
* This will filter out all non truthy values
*/
const truthy_1 = require('./truthy');
function arrayDefalsy(array) {
  return array.filter(truthy_1.truthy);
}
exports.arrayDefalsy = arrayDefalsy;
