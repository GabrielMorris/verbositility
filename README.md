[![Build Status](https://travis-ci.org/GabrielMorris/verbositility.svg?branch=master)](https://travis-ci.org/GabrielMorris/verbositility)

# Verbositility

Verbosility is a general purpose Javascript utility library that makes it easier to do stuff functionally!

## Why Verbositility?

That's a great question! There's probably no reason to use this over Lodash or Underscore.js, I'm just building it for myself to help get comfortable with functional programming!
