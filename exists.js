'use strict';
/*
* Function that returns true if x exists
* and false if it does not
*/
Object.defineProperty(exports, '__esModule', { value: true });
function exists(x) {
  return x !== null && x !== undefined;
}
exports.exists = exists;
