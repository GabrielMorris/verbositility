'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
/*
* This repeats a string a specified number of times
* repeatString('h', 3) => 'hhh'
*/
const repeater_1 = require('./repeater');
function repeatString(string, times) {
  return repeater_1
    .repeater(times, function() {
      return string;
    })
    .join('');
}
exports.repeatString = repeatString;
